" local syntax file - set colors on a per-machine basis:
" vim: tw=0 ts=4 sw=4
" Vim color file

set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name="syslak"
hi Normal		guifg=None		guibg=None
hi String		guifg=#FF3FF7
hi Comment		guifg=#959595
hi Constant		guifg=#06d5FF	gui=bold
hi Special		guifg=#A2A2A2
hi Type			guifg=#00FF00	gui=bold 
hi Identifier	guifg=#82ff60	gui=bold
hi Visual		guifg=#EEEEEE	guibg=#999999
hi Include		guifg=#82C3FF	
hi PreCondit	guifg=#FAED72
hi Macro		guifg=#FFF2CC
hi Statement    guifg=#CA4879	gui=bold
hi PreProc		guifg=#70BBFF
hi Special		guifg=Cyan

" hi Typedef		guifg=#F92672     

"hi NonText		guifg=yellow guibg=#303030
"hi statement	guifg=lightblue	gui=NONE
"hi preproc		guifg=Pink2
"hi ErrorMsg		guifg=Black	guibg=Red
"hi WarningMsg	guifg=Black	guibg=Green
"hi Error		guibg=Red
"hi Todo			guifg=Black	guibg=orange
"hi Cursor		guibg=#60a060 guifg=#00ff00
"hi Search		guibg=darkgray guifg=black gui=bold 
"hi IncSearch	gui=NONE guibg=steelblue
"hi LineNr		guifg=darkgrey
" hi title		guifg=darkgrey
"hi ShowMarksHL ctermfg=cyan ctermbg=lightblue cterm=bold guifg=yellow guibg=black  gui=bold
"hi StatusLineNC	gui=NONE guifg=lightblue guibg=darkblue
"hi StatusLine	gui=bold	guifg=cyan	guibg=blue
"hi label		guifg=gold2
"hi operator		guifg=orange
"hi clear Visual
"hi DiffChange   guibg=darkgreen
"hi DiffText		guibg=olivedrab
"hi DiffAdd		guibg=slateblue
"hi DiffDelete   guibg=coral
"hi Folded		guibg=gray30
"hi FoldColumn	guibg=gray30 guifg=white
"hi cIf0			guifg=gray
"hi diffOnly	guifg=red gui=bold
